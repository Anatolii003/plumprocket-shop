<?php

class HomeController
{

	public function actionIndex()
	{
		$style = '<link rel="stylesheet" type="text/css" href="../../assets/styles/main_styles.css">
					<link rel="stylesheet" type="text/css" href="../../assets/styles/responsive.css">
					<link rel="stylesheet" type="text/css" href="../../vue-components/products/products.css">
					<link rel="stylesheet" type="text/css" href="../../vue-components/add-product/add-product.css">';

		$script = '<script src="../../vue-components/products/products.js"></script>
					<script src="../../vue-components/add-product/add-product.js"></script>
					<script src="../../vue-components/home/home.js"></script>';

		$categoryList = Category::getCategoryList();

		require_once( ROOT . '/views/home/index.php' );

		return true;
	}

}