<?php


class CategoryController
{

	public function actionIndex($categorySlug)
	{
		$style = '<link rel="stylesheet" type="text/css" href="../../assets/styles/categories.css">
			<link rel="stylesheet" type="text/css" href="../../assets/styles/categories_responsive.css">
					<link rel="stylesheet" type="text/css" href="../../vue-components/products/products.css">
					<link rel="stylesheet" type="text/css" href="../../vue-components/add-product/add-product.css">';

		$script = '<script src="../../vue-components/products/products.js"></script>
					<script src="../../vue-components/add-product/add-product.js"></script>
					<script src="../../vue-components/category/category.js"></script>';

		$categoryList = Category::getCategoryList();

		$categoryId = Category::getCategoryBySlug($categorySlug);

		$productList = Product::getProductListByCategory($categoryId);

		require_once ROOT . '/views/category/index.php';

		return true;
	}

	public function actionGetCategories()
	{
		$categoryList = Category::getCategoryList();

		echo json_encode($categoryList);

		return true;
	}

}