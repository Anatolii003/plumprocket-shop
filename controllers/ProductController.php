<?php


class ProductController
{
	protected static int $products_per_page = 12;

	public function actionIndex( $productId )
	{
		$style = '<link rel="stylesheet" type="text/css" href="../../assets/styles/product.css">
				<link rel="stylesheet" type="text/css" href="../../assets/styles/product_responsive.css">
				<link rel="stylesheet" type="text/css" href="../../vue-components/single-product/single-product.css">';

		$script = '<script src="../../vue-components/products/products.js"></script>
					<script src="../../vue-components/single-product/product.js"></script>
					<script src="../../vue-components/single-product/single-product.js"></script>';

		$categoryList = Category::getCategoryList();

		$product = Product::getProductById( $productId );

		require_once ROOT . '/views/product/index.php';

		return true;
	}

	public function actionSearch()
	{
		$productList = [];

		$style = '<link rel="stylesheet" type="text/css" href="../../assets/styles/categories.css">
			<link rel="stylesheet" type="text/css" href="../../assets/styles/categories_responsive.css">';

		$script = '<script src="../../vue-components/products/products.js"></script>
					<script src="../../vue-components/home/home.js"></script>';

		if ( isset( $_POST['search'] ) ) {
			$searchQuery = $_POST['search'];

//			$productList = Product::getProductListBySearch($searchQuery);
		}

		$categoryList = Category::getCategoryList();

		require_once ROOT . '/views/product/search-page.php';

		return true;
	}

	public function actionGetProductListBySearch()
	{
		$query = $_POST['query'];

		$productList = Product::getProductListBySearch($query);

		echo json_encode($productList);

		return true;
	}

	public function actionGetSingleProduct($id)
	{
		$product = Product::getProductById($id);

		echo json_encode($product);

		return true;
	}

	public function actionEditProduct($id)
	{
		$options = $_POST;

		$product = Product::updateProductById($id, $options);

		if ( $_FILES["image"] ) {
			if (is_uploaded_file($_FILES["image"]["tmp_name"])) {
				move_uploaded_file(
					$_FILES["image"]["tmp_name"],
					$_SERVER['DOCUMENT_ROOT'] . "/upload/images/product/" . $id . '.jpg'
				);
			}
		} else {
			array_push($error, 'unable to create image' );
		}

		echo json_encode($product);

		return true;
	}

	public function actionDeleteProduct($id)
	{
		$product = Product::deleteProductById($id);
		if ($product) {
			unlink( $_SERVER['DOCUMENT_ROOT'] . "/upload/images/product/" . $id . '.jpg' );
		}

		echo json_encode($product);

		return true;
	}

	public function actionGetProducts()
	{
		$productList = Product::getLatestProductHome( self::$products_per_page );

		echo json_encode($productList);

		return true;
	}

	public function actionGetProductsByCategory($categorySlug)
	{
		$categoryId = Category::getCategoryBySlug($categorySlug);
		$productList = Product::getProductListByCategory($categoryId);

		echo json_encode($productList);

		return true;
	}

	public function actionCreateProduct()
	{
		$error = [];
		$image_name = $_FILES["image"]["name"];
		preg_match('/\.\w+/', $image_name, $image_extension);

		$response = Product::createProduct( $_POST, $image_extension );

		if ( !$response ){
			array_push($error, 'unable to create image' );
		}

		if ( $response && $_FILES["image"] ) {
			if (is_uploaded_file($_FILES["image"]["tmp_name"])) {
				move_uploaded_file(
					$_FILES["image"]["tmp_name"],
					$_SERVER['DOCUMENT_ROOT'] . "/upload/images/product/" . $response . '.jpg'
				);
			}
		} else {
			array_push($error, 'unable to create image' );
		}

		echo json_encode($error);

		return true;
	}

}