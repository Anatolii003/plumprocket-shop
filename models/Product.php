<?php

class Product
{

	const AMOUNT_IN_HOME = 4;

	public static function getLatestProductHome($amount = self::AMOUNT_IN_HOME)
	{
		$amount = intval( $amount );

		$db = Db::getConnection();

		$product = array();

		$result = $db->query( 'SELECT id, name, price, date FROM product ORDER BY id DESC LIMIT ' . $amount );

		$i = 0;
		while ( $row = $result->fetch() ) {
			$product[ $i ]['id'] = $row['id'];
			$product[ $i ]['name'] = $row['name'];
			$product[ $i ]['price'] = $row['price'];
			$product[ $i ]['date'] = $row['date'];
			$i++;
		}

		return $product;

	}

	public static function getProductListByCategory( $categoryId = false )
	{
		$product = array();

		if ( $categoryId ) {
			$db = Db::getConnection();

			$categoryId = $categoryId['id'];

			$result = $db->query( "SELECT id, name, price, date FROM product WHERE categoryId = '$categoryId' ORDER BY id DESC" );

			$i = 0;
			while ( $row = $result->fetch() ) {
				$product[ $i ]['id'] = $row['id'];
				$product[ $i ]['name'] = $row['name'];
				$product[ $i ]['price'] = $row['price'];
				$product[ $i ]['date'] = $row['date'];
				$i++;
			}
		}

		return $product;
	}

	public static function getProductListBySearch( $searchQuery = false )
	{
		$product = array();

		if ( $searchQuery ) {
			$db = Db::getConnection();


			$result = $db->query( "SELECT id, name, price, date FROM product
				WHERE name LIKE '%" . $searchQuery . "%'
				OR description LIKE '%" . $searchQuery . "%'
				OR sku LIKE '%" . $searchQuery . "%'
				ORDER BY id DESC" );

			$i = 0;
			while ( $row = $result->fetch() ) {
				$product[ $i ]['id'] = $row['id'];
				$product[ $i ]['name'] = $row['name'];
				$product[ $i ]['price'] = $row['price'];
				$product[ $i ]['date'] = $row['date'];
				$i++;
			}
		}

		return $product;
	}

	public static function getProductById( $id )
	{
		$id = intval( $id );
		if ( $id ) {
			$db = Db::getConnection();

			$result = $db->query( 'SELECT * FROM product WHERE id = ' . $id );

			$result->setFetchMode( PDO::FETCH_ASSOC );

			return $result->fetch();
		}
	}

	public static function createProduct( $options )
	{
		$db = Db::getConnection();

		$sql = 'INSERT INTO product '
			. '(name, sku, price, categoryId,'
			. 'description)'
			. 'VALUES '
			. '(:name, :sku, :price, :categoryId,'
			. ':description)';

		$result = $db->prepare( $sql );
		$result->bindParam( ':name', $options['name'], PDO::PARAM_STR );
		$result->bindParam( ':sku', $options['sku'], PDO::PARAM_STR );
		$result->bindParam( ':price', $options['price'], PDO::PARAM_STR );
		$result->bindParam( ':categoryId', $options['categoryId'], PDO::PARAM_INT );
		$result->bindParam( ':description', $options['description'], PDO::PARAM_STR );
		if ( $result->execute() ) {
			return $db->lastInsertId();
		}
		return 0;
	}

	public static function updateProductById($id, $options)
	{
		$db = Db::getConnection();

		$sql = "UPDATE product
            SET 
                name = :name, 
                sku = :sku, 
                price = :price, 
                categoryId = :categoryId,
                description = :description
            WHERE id = :id";

		$result = $db->prepare($sql);
		$result->bindParam(':id', $id, PDO::PARAM_INT);
		$result->bindParam(':name', $options['name'], PDO::PARAM_STR);
		$result->bindParam(':sku', $options['sku'], PDO::PARAM_STR);
		$result->bindParam(':price', $options['price'], PDO::PARAM_STR);
		$result->bindParam(':categoryId', $options['categoryId'], PDO::PARAM_INT);
		$result->bindParam(':description', $options['description'], PDO::PARAM_STR);
		return $result->execute();
	}

	public static function deleteProductById($id)
	{
		$db = Db::getConnection();

		$sql = 'DELETE FROM product WHERE id = :id';

		$result = $db->prepare($sql);
		$result->bindParam(':id', $id, PDO::PARAM_INT);
		return $result->execute();
	}

}