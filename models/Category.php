<?php


class Category
{

	public static function getCategoryList()
	{
		$db = Db::getConnection();

		$categoryList = array();

		$result = $db->query( 'SELECT id, name, slug FROM categories' );

		$i = 0;
		while ( $row = $result->fetch() ) {
			$categoryList[ $i ]['id'] = $row['id'];
			$categoryList[ $i ]['name'] = $row['name'];
			$categoryList[ $i ]['slug'] = $row['slug'];
			$i++;
		}

		return $categoryList;
	}

	public static function getCategoryBySlug($categorySlug)
	{
		$db = Db::getConnection();

		$result = $db->query( "SELECT * FROM categories WHERE slug = '$categorySlug'" );

		$result->setFetchMode( PDO::FETCH_ASSOC );

		return $result->fetch();
	}

}