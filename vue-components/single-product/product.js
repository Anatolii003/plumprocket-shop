let product = Vue.component( 'product', {
	data() {
		return {
			product: [],
			categories: [],
			editStatus: false,
			image: '',
			replaceImage: '',
		}
	},
	props: {
		productId: {
			default: ''
		}
	},
	computed: {
		nameCheck() {
			if ( !this.product.name ) {
				return true
			}
		},
		skuCheck(){
			if ( !this.product.sku ) {
				return true
			}
		},
		priceCheck(){
			if ( !this.product.price ) {
				return true
			}
		},
		descriptionCheck(){
			if ( !this.product.description ) {
				return true
			}
		},
		categoryCheck(){
			if ( !this.product.categoryId ) {
				return true
			}
		},
		imageCheck(){
			if ( !this.product.image ) {
				return true
			}
		},
	},
	methods: {
		getData() {
			let self = this;
			axios.get( window.location.origin + '/rest/product/get-product/' + self.productId )
			.then( function ( response ) {
				self.product = response.data
			} )
			.catch( error => console.log( error ) );
		},
		getCategoryList(){
			let self = this
			axios.get( window.location.origin + '/rest/category/get-list' )
			.then( response => (self.categories = response.data) )
			.catch( error => console.log( error ) );
		},
		editProduct( id ) {
			console.log( id );
			this.editStatus = true;
		},
		saveProduct(id){
			this.editStatus = false;
			let self = this;

			let data = new FormData();

			data.append('image', this.image);
			data.append('name', this.product.name);
			data.append('sku', this.product.sku);
			data.append('price', this.product.price);
			data.append('description', this.product.description);
			data.append('categoryId', this.product.categoryId);

			axios.post( window.location.origin + '/rest/product/edit-product/' + self.product.id, data )
			.then( function ( response ) {
				console.log( response );
			} )
			.catch( error => console.log( error ) );

		},
		onFileChange(e){
			let self = this;
			let files = e.target.files;
			this.image = files[0];

			if (files && files[0]) {
				let reader = new FileReader();
				reader.onload = (event) => {
					self.replaceImage = event.target.result
				}
				reader.readAsDataURL(files[0]);
			}
		},
		deleteProduct( id ) {
			this.$parent.notification = 'Ви впевнені що хочете видалити продукт ' + this.product.name;
			this.$parent.notificationIsActive = true;
			this.$parent.notificationBackground = '#E73B00';
		},
		confirmDelete( message ) {
			if ( message ) {
				let self = this;
				axios.delete( window.location.origin + '/rest/product/delete-product/' + self.product.id )
				.then( function ( response ) {
					console.log( response );
					self.product = response.data
					window.history.back();
				} )
				.catch( error => console.log( error ) );
			}
		}
	},
	mounted() {
		this.getData();
		this.getCategoryList();
		this.$root.$on( 'confirm-delete', this.confirmDelete );
	},
	template: `
    <div class="row details_row">
        <div class="col-12 d-flex">
            <button type="button" class="btn btn-danger mb-5" @click="deleteProduct(product.id)">
            Видалити продукт
            </button>
            <button v-show="!editStatus" type="button" class="btn btn-success ml-5 mb-5" @click="editProduct(product.id)">
                Редагувати продукт
            </button>
            <button v-show="editStatus" type="button" class="btn btn-primary ml-5 mb-5" @click="saveProduct(product.id)">
            Зберегти дані
            </button>
        </div>
        <!-- Product Image -->
        <div class="col-lg-6">
            <div class="details_image">
                <div class="details_image_large">
                <img v-show="!replaceImage" :src="'../../upload/images/product/' + product.id + '.jpg'" alt="">
                <img v-show="replaceImage" :src="replaceImage" alt="">
<!--                    <div class="product_extra product_new">-->
<!--                        <a href="categories.html">New</a>-->
<!--                    </div>-->
                </div>
            </div>
            <div class="file_upload" v-show="editStatus">
                <button type="button" class="btn btn-success">
                    Змінити зображення
                </button>
                <input
                    type="file"
                    @change="onFileChange"
                >
            </div>
        </div>

        <!-- Product Content -->
        <div class="col-lg-6">
            <div class="details_content">
                <div class="details_name">
                    <div v-show="!editStatus">{{ product.name }}</div>
                    <div v-show="editStatus" class="input-group mb-3">
                        <div class="input-group-prepend">
                          <span class="input-group-text">Назва</span>
                        </div>
                        <input
                          type="text"
                          class="form-control"
                          :class="{'is-invalid': nameCheck}"
                          placeholder="Введіть назву продукту"
                          aria-label="Username"
                          aria-describedby="basic-addon1"
                          v-model="product.name"
                        >
                        <div class="invalid-feedback" v-if="nameCheck">
                          Введіть будь ласка назву продукту
                        </div>
                    </div>
                </div>
                <!--                    <div class="details_discount">$890</div>-->
                <div class="details_price">
                    <div v-show="!editStatus">{{ product.price }} ₴</div>
                    <div v-show="editStatus" class="input-group mb-3">
                        <div class="input-group-prepend">
                          <span class="input-group-text input-group-prepend__price">Ціна</span>
                          <span class="input-group-text input-group-prepend__sign">₴</span>
                        </div>
                        <input
                          type="number"
                          class="form-control"
                          :class="{'is-invalid': priceCheck}"
                          placeholder="Введіть ціну продукту"
                          aria-label="Username"
                          aria-describedby="basic-addon1"
                          v-model="product.price"
                        >
                        <div class="invalid-feedback" v-if="priceCheck">
                          Введіть будь ласка ціну
                        </div>
                    </div>
                </div>

                <!-- In Stock -->
                <!--                    <div class="in_stock_container">-->
                <!--                        <div class="availability">Availability:</div>-->
                <!--                        <span>In Stock</span>-->
                <!--                    </div>-->

                <div class="details_text">
                    <div v-show="!editStatus"><p>{{ product.description }}</p></div>
                    <div v-show="editStatus" class="input-group mb-3">
                        <div class="input-group-prepend">
                          <span class="input-group-text">Опис</span>
                        </div>
                        <input
                          type="text"
                          class="form-control"
                          :class="{'is-invalid': descriptionCheck}"
                          placeholder="Введіть опис продукту"
                          aria-label="Username"
                          aria-describedby="basic-addon1"
                          v-model="product.description"
                        >
                        <div class="invalid-feedback" v-if="descriptionCheck">
                          Введіть будь ласка опис
                        </div>
                    </div>
                </div>

                <div class="in_stock_container">
                    <div class="availability">
                        Дата створення: {{ product.date }}
                    </div>
                </div>

                <div v-show="editStatus" class="in_stock_container">
                    <div class="input-group mb-3">
                        <div class="input-group-prepend">
                          <label class="input-group-text" for="inputGroupSelect01">Категорія</label>
                        </div>
                        <select
                          class="custom-select"
                          :class="{'is-invalid': categoryCheck}"
                          id="inputGroupSelect01"
                          v-model="product.categoryId"
                        >
                          <option selected disabled value="">Виберіть категорію...</option>
                          <option :value="category.id" v-for="category in categories">{{ category.name }}</option>
                        </select>
                        <div class="invalid-feedback" v-if="categoryCheck">
                          Виберіть будь ласка категорію
                        </div>
                    </div>
                </div>

                <div class="details_text">
                    <div v-show="!editStatus" class="availability">
                        Артикул: {{ product.sku }}
                    </div>
                    <div v-show="editStatus" class="input-group mb-3">
                        <div class="input-group-prepend">
                          <span class="input-group-text">Артикул</span>
                        </div>
                        <input
                          type="text"
                          class="form-control"
                          :class="{'is-invalid': skuCheck}"
                          placeholder="Введіть артикул"
                          aria-label="Username"
                          aria-describedby="basic-addon1"
                          v-model="product.sku"
                        >
                        <div class="invalid-feedback" v-if="skuCheck">
                          Введіть будь ласка артикул
                        </div>
                    </div>
                </div>

                <!-- Product Quantity -->
                <div class="product_quantity_container">
                    <div class="product_quantity clearfix">
                        <span>Qty</span>
                        <input id="quantity_input" type="text" pattern="[0-9]*" value="1">
                        <div class="quantity_buttons">
                            <div id="quantity_inc_button" class="quantity_inc quantity_control"><i
                                    class="fa fa-chevron-up" aria-hidden="true"></i></div>
                            <div id="quantity_dec_button" class="quantity_dec quantity_control"><i
                                    class="fa fa-chevron-down" aria-hidden="true"></i></div>
                        </div>
                    </div>
                    <div class="button cart_button"><a href="#">Add to cart</a></div>
                </div>

                <!-- Share -->
                <div class="details_share">
                    <span>Share:</span>
                    <ul>
                        <li><a href="#"><i class="fa fa-pinterest" aria-hidden="true"></i></a></li>
                        <li><a href="#"><i class="fa fa-instagram" aria-hidden="true"></i></a></li>
                        <li><a href="#"><i class="fa fa-facebook" aria-hidden="true"></i></a></li>
                        <li><a href="#"><i class="fa fa-twitter" aria-hidden="true"></i></a></li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
	`,
} );