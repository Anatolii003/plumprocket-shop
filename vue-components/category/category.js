new Vue( {
	el: '#super_container',
	data() {
		return {
			notification: '',
			notificationIsActive: false,
		};
	},
	components: {
		'products': products,
		'add-product': add_product,
		'notification': notification,
	}
} );

Vue.prototype.$eventBus = new Vue();