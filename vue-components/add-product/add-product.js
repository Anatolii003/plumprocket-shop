let add_product = Vue.component('add-product', {
	data(){
		return{
			categories: [],
			name: '',
			sku: '',
			price: '',
			description: '',
			category: '',
			image: '',
			files: [],
			submitStatus: false,
			imgPreview: '',
		}
	},
	props: {
		categorySlug: {
			default: ''
		}
	},
	computed: {
		nameCheck(){
			if ( !this.name && this.submitStatus ) {
				return true
			}
		},
		skuCheck(){
			if ( !this.sku && this.submitStatus ) {
				return true
			}
		},
		priceCheck(){
			if ( !this.price && this.submitStatus ) {
				return true
			}
		},
		descriptionCheck(){
			if ( !this.description && this.submitStatus ) {
				return true
			}
		},
		categoryCheck(){
			if ( !this.category && this.submitStatus ) {
				return true
			}
		},
		imageCheck(){
			if ( !this.image && this.submitStatus ) {
				return true
			}
		},
	},
	methods: {
		saveData(){
			this.submitStatus = true;
			let self = this;

			if ( !this.nameCheck && !this.skuCheck && !this.priceCheck && !this.descriptionCheck && !this.categoryCheck && !this.imageCheck ) {
				let data = new FormData();

				data.append('image', this.image);
				data.append('name', this.name);
				data.append('sku', this.sku);
				data.append('price', this.price);
				data.append('description', this.description);
				data.append('categoryId', this.category);

				axios.post(window.location.origin + '/rest/product/create-product', data)
				.then( function ( response ) {
					if ( response.data.length === 0 ) {
						axios
						.get( window.location.origin + '/rest/product/get-list/' + self.categorySlug )
						.then( (response) => {
							console.log(response);
							self.$root.$emit('reloadData' , response.data);
						} )
						.catch( error => console.log( error ) );
						$('#exampleModal').modal('hide');
						self.$parent.notification = 'Продукт створено';
						self.$parent.notificationIsActive = true;
					} else {
						console.log(response.data);
					}
				})
				.catch( error => console.log( error ) );

			}
		},
		onFileChange(e){
			let self = this;
			let files = e.target.files;
			this.image = files[0];

			if (files && files[0]) {
				let reader = new FileReader();
				reader.onload = (event) => {
					self.imgPreview = event.target.result
				}
				reader.readAsDataURL(files[0]);
			}
		},
	},
	mounted(){
		axios
		.get( window.location.origin + '/rest/category/get-list' )
		.then( response => (this.categories = response.data) )
		.catch( error => console.log( error ) );
	},
	template:`
	<div class="container">
      <div class="row">
        <div class="col">
          <button type="button" class="btn btn-outline-secondary" data-toggle="modal" data-target="#exampleModal">
            Створити продукт
          </button>
          
          <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
            <div class="modal-dialog" role="document">
              <div class="modal-content">
                <div class="modal-header">
                  <h2 class="modal-title" id="exampleModalLabel">Створити продукт</h2>
                  <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                  </button>
                </div>
                <div class="modal-body">
                  <div class="input-group mb-3">
                    <div class="input-group-prepend">
                      <span class="input-group-text">Назва</span>
                    </div>
                    <input
                      type="text"
                      class="form-control"
                      :class="{'is-invalid': nameCheck}"
                      placeholder="Введіть назву продукту"
                      aria-label="Username"
                      aria-describedby="basic-addon1"
                      v-model="name"
                    >
                    <div class="invalid-feedback" v-if="nameCheck">
                      Введіть будь ласка назву продукту
                    </div>
                  </div>
                  <div class="input-group mb-3">
                    <div class="input-group-prepend">
                      <span class="input-group-text">Артикул</span>
                    </div>
                    <input
                      type="text"
                      class="form-control"
                      :class="{'is-invalid': skuCheck}"
                      placeholder="Введіть артикул"
                      aria-label="Username"
                      aria-describedby="basic-addon1"
                      v-model="sku"
                    >
                    <div class="invalid-feedback" v-if="skuCheck">
                      Введіть будь ласка артикул
                    </div>
                  </div>
                  <div class="input-group mb-3">
                    <div class="input-group-prepend">
                      <span class="input-group-text input-group-prepend__price">Ціна</span>
                      <span class="input-group-text input-group-prepend__sign">₴</span>
                    </div>
                    <input
                      type="number"
                      class="form-control"
                      :class="{'is-invalid': priceCheck}"
                      placeholder="Введіть ціну продукту"
                      aria-label="Username"
                      aria-describedby="basic-addon1"
                      v-model="price"
                    >
                    <div class="invalid-feedback" v-if="priceCheck">
                      Введіть будь ласка ціну
                    </div>
                  </div>
                  <div class="input-group mb-3">
                    <div class="input-group-prepend">
                      <span class="input-group-text">Опис</span>
                    </div>
                    <input
                      type="text"
                      class="form-control"
                      :class="{'is-invalid': descriptionCheck}"
                      placeholder="Введіть опис продукту"
                      aria-label="Username"
                      aria-describedby="basic-addon1"
                      v-model="description"
                    >
                    <div class="invalid-feedback" v-if="descriptionCheck">
                      Введіть будь ласка опис
                    </div>
                  </div>
                  <div class="input-group mb-3">
                    <div class="input-group-prepend">
                      <label class="input-group-text" for="inputGroupSelect01">Категорія</label>
                    </div>
                    <select
                      class="custom-select"
                      :class="{'is-invalid': categoryCheck}"
                      id="inputGroupSelect01"
                      v-model="category"
                    >
                      <option selected disabled value="">Виберіть категорію...</option>
                      <option :value="category.id" v-for="category in categories">{{ category.name }}</option>
                    </select>
                    <div class="invalid-feedback" v-if="categoryCheck">
                      Виберіть будь ласка категорію
                    </div>
                  </div>
                  <div class="input-group mb-3">
                    <div class="input-group-prepend">
                      <span class="input-group-text" id="inputGroupFileAddon01">Зображення</span>
                    </div>
                    <div class="custom-file" :class="{'is-invalid': imageCheck}">
                      <input
                        type="file"
                        name="image"
                        class="custom-file-input"
                        id="imgInput"
                        aria-describedby="inputGroupFileAddon01"
                        @change="onFileChange"
                      >
                      <label class="custom-file-label" for="inputGroupFile01">Виберіть зображення</label>
                    </div>
                    <div class="invalid-feedback" v-if="imageCheck">
                      Виберіть будь ласка зображення
                    </div>
                  </div>
                  <div class="input-group__img-preview">
                      <img id="image" :src="imgPreview" alt="" />
                  </div>
                </div>
                <div class="modal-footer">
                  <button type="button" class="btn btn-secondary" data-dismiss="modal">Відмінити</button>
                  <button type="button" class="btn btn-primary" @click="saveData">Зберегти</button>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
	</div>
	`,
});