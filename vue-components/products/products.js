let products = Vue.component( 'products', {
	data() {
		return {
			productList: [],
		};
	},
	computed: {
		countProducts(){
			if ( this.productList.length === 1 ) {
				return 'результат';
			} else if ( this.productList.length === 2 || this.productList.length === 3 || this.productList.length === 4 ) {
				return 'результати';
			} else {
				return 'результатів';
			}
		}
	},
	props: {
		categorySlug: {
			default: '',
		},
		searchQuery: {
			default: '',
		},
	},
	methods: {
		getData(){
			let self = this;
			if (this.searchQuery) {
				console.log(this.searchQuery);

				let data = new FormData();

				data.append('query', this.searchQuery);

				axios.post( window.location.origin + '/rest/product/search/get-list', data)
				.then( function ( response ) {
					self.productList = response.data
				} )
				.catch( error => console.log( error ) );
			} else {
				axios.get( window.location.origin + '/rest/product/get-list/' + self.categorySlug )
				.then( function ( response ) {
					console.log( response );
					self.productList = response.data
				} )
				.catch( error => console.log( error ) );
			}
		},
		reload(message){
			this.productList = message;
		},
		price_asc(){
			this.productList.sort(function(a, b){
				return a.price-b.price
			})
		},
		price_desc(){
			this.productList.sort(function(a, b){
				return b.price-a.price
			})
		},
		name_asc(){
			this.productList.sort(function(a, b){
				let nameA=a.name.toLowerCase(), nameB=b.name.toLowerCase()
				if (nameA < nameB)
					return -1
				if (nameA > nameB)
					return 1
				return 0
			})
		},
		name_desc(){
			this.productList.sort(function(a, b){
				let nameA=a.name.toLowerCase(), nameB=b.name.toLowerCase()
				if (nameA > nameB)
					return -1
				if (nameA < nameB)
					return 1
				return 0
			})
		},
		date_desc(){
			this.productList.sort(function(a, b){
				let dateA=new Date(a.date), dateB=new Date(b.date)
				return dateB-dateA
			})
		},
		date_asc(){
			this.productList = this.productList.sort(function(a, b){
				let dateA=new Date(a.date), dateB=new Date(b.date)
				return dateA-dateB
			})
		},
	},
	mounted() {
		this.getData()
		this.$root.$on( 'reloadData' , this.reload);
	},
	template: `
        <div class="container">
        <div class="row">
            <div class="col">

                <!-- Product Sorting -->
                <div class="sorting_bar d-flex flex-md-row flex-column align-items-md-center justify-content-md-start">
                    <div class="results">Показано <span>{{ productList.length }}</span> {{ countProducts }}
                    </div>
                    <div class="sorting_container ml-md-auto">
                        <div class="sorting">
                            <ul class="item_sorting">
                                <li>
                                    <span class="sorting_text">Сортувати</span>
                                    <i class="fa fa-chevron-down" aria-hidden="true"></i>
                                    <ul>
                                        <li class="product_sorting_btn"
                                            @click="price_asc"
                                        >
                                            <span>За зростанням ціни</span>
                                        </li>
                                        <li class="product_sorting_btn"
                                            @click="price_desc"
                                        >
                                            <span>За зменшенням ціни</span>
                                        </li>
                                        <li class="product_sorting_btn"
                                            @click="name_asc"
                                        >
                                            <span>За назвою A-Z А-Я</span>
                                        </li>
                                        <li class="product_sorting_btn"
                                            @click="name_desc"
                                        >
                                            <span>За назвою Я-А Z-A</span>
                                        </li>
                                        <li class="product_sorting_btn"
                                            @click="date_desc"
                                        >
                                            <span>Спочатку найновіші</span>
                                        </li>
                                        <li class="product_sorting_btn"
                                            @click="date_asc"
                                        >
                                            <span>Найновіші в кінці</span>
                                        </li>
                                    </ul>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col">
                <div class="product_grid row">
                    <div class="col-xl-3 col-lg-4 product" v-for="item in productList">
                        <div class="product_image">
                            <img :src="'../../upload/images/product/' + item.id + '.jpg'" alt="">
                        </div>
                            <div class="product_extra product_new"><a href="categories.html">New</a></div>
                            <div class="product_content">
                                <div class="product_title">
                                    <a :href="'/product/' + item.id">
                                        {{ item.name }}
                                    </a>
                                </div>
                                <div class="product_price">{{ item.price }}₴</div>
                            <div class="product_date">{{ item.date }}</div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
      </div>
	`,
} );