let notification = Vue.component('notification', {
	template:`
  <div class="notification" :style="{'background-color': $parent.notificationBackground}" :class="{active: $parent.notificationIsActive}">
      <div class="notification__top">
          <strong>{{ $parent.notification }}</strong>
              <button type="button" class="close" aria-label="Close" @click="$parent.notificationIsActive = false">
                  <span aria-hidden="true">&times;</span>
              </button>
      </div>
      <div class="notification__body mt-5 d-flex justify-content-between" v-if="$parent.notificationBackground">
          <button
              type="button"
              class="btn btn-secondary"
              @click="[$root.$emit('confirm-delete', true), $parent.notificationIsActive = false]">
               Підтвердити
          </button>
          <button type="button" class="btn btn-secondary" @click="$parent.notificationIsActive = false">
               Відмінити
          </button>
      </div>
  </div>
	`,
});