new Vue( {
	el: '#super_container',
	data() {
		return {
			notification: '',
			notificationIsActive: false,
		};
	},
	components: {
		'products': products,
		'notification': notification,
	}
} );

Vue.prototype.$eventBus = new Vue();