<?php

function loader( $classname )
{
	$array_paths = array(
		'/models/',
		'/components/'
	);

	foreach ( $array_paths as $path ) {
		$filename = ROOT . $path . $classname . '.php';
		if ( file_exists( $filename ) ) {
			include_once $filename;
		}
	}
}

spl_autoload_register( 'loader' );