<?php

return array(
	'rest/product/create-product' => 'product/createProduct',
	'rest/product/get-list/([\w ]+)' => 'product/getProductsByCategory/$1',
	'rest/product/get-list' => 'product/getProducts',
	'rest/product/search/get-list' => 'product/getProductListBySearch',
	'rest/product/get-product/([0-9]+)' => 'product/getSingleProduct/$1',
	'rest/product/delete-product/([0-9]+)' => 'product/deleteProduct/$1',
	'rest/product/edit-product/([0-9]+)' => 'product/editProduct/$1',
	'rest/category/get-list' => 'category/getCategories',

	'search' => 'product/search',
	'product/([0-9]+)' => 'product/index/$1',
	'category/([\w ]+)' => 'category/index/$1',
	'' => 'home/index',
);